import axios from '../../axios';

export default{
    registerUser(ctx, payload){
        return new Promise((resolve, reject)=> {
          axios.post('register', payload)
          .then((response)=>{
            if(response.data){
                resolve(response);
            }else{
                reject(response)
            }
          })
          .catch((error)=>{
              reject(error)
          })
        })
    },
    loginUser(ctx, payload){
        return new Promise((resolve, reject)=> {
          axios.post('login', payload)
          .then((response)=>{
            console.log(response.data)
            localStorage.setItem('token', response.data.access_token)

            ctx.commit('setLoggedIn', true);
            resolve(response);
            
          })
          .catch((error)=>{
              reject(error)
          })
        })
    },
    logoutUser(ctx){
        return new Promise((resolve) => {
            localStorage.removeItem('token')
            localStorage.removeItem('user_role')
            ctx.commit('setLoggedIn', false)
            resolve(true)
        })
    },
    setLoggedInState(ctx){
        return new Promise((resolve) => {
            if(localStorage.getItem('token')){
                ctx.commit('setLoggedIn', true)
                resolve(true)
            }else{
                ctx.commit('setLoggedIn', false)
                resolve(false)
            }
        })
    },
    me(ctx){
        return new Promise((resolve, reject) => {
            axios.get('me')
            .then((response) => {
                localStorage.setItem('user_role', JSON.stringify(response.data.roles))
                ctx.commit('setUserDetails', response.data)
                resolve(response.data)
                console.log(response.data)
            }).catch((error) =>{
                reject(error)
            })
        })
    }
}