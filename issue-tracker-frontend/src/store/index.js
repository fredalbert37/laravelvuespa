import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

//modules
import user from './user/index'
import application from './application/index'


export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    user,
    application
  }
})
