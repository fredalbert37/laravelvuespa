import Vue from 'vue'
import VueRouter from 'vue-router'
import Middlewares from '../middlewares/index'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirectTo: '/login',
    component: () => import(/* webpackChunkName: "login" */ '../views/auth/Login.vue'),
  },
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "login" */ '../views/auth/Login.vue'),
    meta: {
      middleware: [Middlewares.guest]
    }
  },
  {
    path: '/register',
    name: 'register',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "register" */ '../views/auth/Register.vue'),
    meta: {
      middleware: [Middlewares.guest]
    }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "dashboard" */ '../views/pages/Dashboard.vue'),
    meta: {
      middleware: [Middlewares.auth ]
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

function nextCheck(context, middleware, index){
    const nextMiddleware = middleware[index];

    if(!nextMiddleware) return context.next;
    
    return (...parameters) => {
      context.next(parameters)
      const nextMidd = nextCheck(context, middleware, index + 1 )

      nextMiddleware({...context, next: nextMidd})
    }

}


router.beforeEach((to, from, next) => {
  if(to.meta.middleware){
    const middleware = Array.isArray(to.meta.middleware) ?  to.meta.middleware: [to.meta.middleware]
  
    const ctx = {
      from,
      next,
      router,
      to
    }

    const nextMiddleware = nextCheck(ctx, middleware, 1)

    return middleware[0]({...ctx, next: nextMiddleware});
  }
  return next();

})

export default router
