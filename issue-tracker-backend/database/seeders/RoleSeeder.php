<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
            [
                "name" => "Company",
                "slug" => "company"
            ],
            [
                "name" => "Admin",
                "slug" => "admin"
            ],
            [
                "name" => "Area",
                "slug" => "area"
            ],
            [
                "name" => "Campus",
                "slug" => "campus"
            ]
        ]);
    }
}
