<?php

namespace App\Http\Controllers\Api;

use App\Actions\Auth\LoginAction;
use App\Actions\Auth\RegisterAction;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    
    public function login(Request $request, LoginAction $loginAction){
        $validate = Validator::make($request->all(), [
            "email" => "required|email",
            "password" => "required"
        ]);

        if($validate->fails()){
            return response()->json([
                'errors' => $validate->errors()
            ]);
        }else{
            
            $passportRequest = $loginAction->run($request);
            $tokenContent = $passportRequest['content'];

            

            if(!empty($tokenContent['access_token'])){
                return $passportRequest['response'];;
            }
            return response()->json([
                'message' => 'Unauthenticated'
            ]);
        }
        
    }

    public function register(Request $request, RegisterAction $registerAction){
        
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        ]);

        if($validate->fails()){
            return response()->json([
                'success' => false,
                'message' => 'Registration failed',
                'errors' => $validate->errors()
            ], 500);
        }else{ 
            

            $user = $registerAction->run($request->all());
    
            return response()->json([
                'success' => true,
                'message' => 'Registration succeeded'
            ]);
        }

        
    }




}
