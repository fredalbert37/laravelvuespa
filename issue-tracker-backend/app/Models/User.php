<?php

namespace App\Models;

use App\Traits\HasPermissions;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasPermissions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(){
        return $this->belongsToMany(Role::class, 'users_roles');
    }


    public function hasRole(...$roles){
        return $this->roles()->whereIn('slug', $roles)->count();
    }

    public function permissions(){
        return $this->belongsToMany(Permission::class, 'users_permissions');
    }
    
    public function user_role($user){
        return DB::table('users_roles')
                ->select('roles.slug')
                ->join('roles', 'users_roles.role_id', 'roles.id')
                ->where('user_id', $user)
                ->first();
    }


}
