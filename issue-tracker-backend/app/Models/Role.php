<?php

namespace App\Models;

use App\Traits\HasPermissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory, HasPermissions;

    public function permissions(){
        return $this->belongsToMany(Permission::class, 'roles_permissions'); 
    }

    public function hasPermissionsTo(...$permisions){
        return $this->permissions()->whereIn('slug', $permisions)->count();
    }

    public function scopeDeveloper($query){
        return $query->where('slug', 'developer');
    }

    public function scopeAdmin($query){
        return $query->where('slug', 'admin');
    }

}
